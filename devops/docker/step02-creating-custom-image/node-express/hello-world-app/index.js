'use strict';

const express = require('express');

// Constants
const APP_NAME = process.env.APP_NAME || "SAMPLE APP";
const VERSION = process.env.VERSION || "NOT DEFINED";
const PORT = 8080;
const HOST = '0.0.0.0';

console.log(`Starting ${APP_NAME}`);
// App
const app = express();
app.get('/', (req, res) => {
  res.send(`Hello world!\nYou accessed "${APP_NAME}"\nVersion: ${VERSION}\n`);
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);